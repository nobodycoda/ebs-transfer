const AWS = require("aws-sdk");

const userTable = "chime-ebs-prod-stack-SchoolDistrct-OI7FKJAHV3MQ";

AWS.config.update({ region: "us-east-1" });
const ddb = new AWS.DynamoDB();

let ticketDetails = [
  {
    city: "Houston",
    id: 2,
    state: "Texas",
    notes: "",
    name: "Houston ISD",
  },
  {
    city: "Allen",
    id: 8,
    state: "Texas",
    notes: "",
    name: "Allen ISD",
  },
  {
    city: "Friendswood",
    id: 9,
    state: "Texas",
    notes: "",
    name: "Friendswood ISD",
  },
  {
    city: "Southlake",
    id: 1,
    state: "Texas",
    notes: "",
    name: "Carroll ISD",
  },
  {
    city: "Rio Grande Valley",
    id: 6,
    state: "Texas",
    notes: "",
    name: "South Texas ISD",
  },
  {
    city: "Irving",
    id: 5,
    state: "Texas",
    notes: "",
    name: "Coppell ISD",
  },
  {
    city: "Wylie",
    id: 4,
    state: "Texas",
    notes: "",
    name: "Lovejoy ISD",
  },
  {
    city: "Lakeway",
    id: 7,
    state: "Tennessee",
    notes: "",
    name: "Lake Travis ISD",
  },
  {
    city: "West Lake Hills",
    id: 3,
    state: "Texas",
    notes: "",
    name: "Eanes ISD",
  },
  {
    city: "Dallas",
    id: 10,
    state: "Texas",
    notes: "",
    name: "Plano ISD",
  },
];

async function insertUsers() {
  let insertPromises = ticketDetails.map(async (participant) => {
    let item = {
      Id: {
        S: participant["id"].toString(),
      },
      Name: {
        S: participant["name"].toString(),
      },
      City: {
        S: participant["city"].toString(),
      },
      State: {
        S: participant["state"].toString(),
      },
      Notes: {
        S: participant["notes"].toString(),
      },
    };

    await ddb
      .putItem({
        TableName: userTable,
        Item: item,
      })
      .promise();
  });

  await Promise.all(insertPromises);

  console.log("Done");
}

insertUsers();
