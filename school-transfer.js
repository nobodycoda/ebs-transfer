const AWS = require("aws-sdk");

const userTable = "chime-ebs-prod-stack-School-GHQP7UABGKQ0";

AWS.config.update({ region: "us-east-1" });
const ddb = new AWS.DynamoDB();

let ticketDetails = [
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Carroll ISD",
    schoolDistrictID: 2,
    students: 12,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "Eubanks Intermediate School",
    state: "Texas",
    id: 2,
    name: "Eubanks Intermediate School",
  },
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Plano ISD",
    schoolDistrictID: 10,
    students: 12,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "Eubanks plano Intermediate School",
    state: "Texas",
    id: 8,
    name: "Eubanks plano Intermediate School",
  },
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Allen ISD",
    schoolDistrictID: 10,
    students: 12,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "Allen Intermediate School",
    state: "Texas",
    id: 9,
    name: "Allen Intermediate School",
  },
  {
    zip: 12345,
    city: "Warener",
    schoolDistrictName: "Carroll ISD",
    students: 12,
    personnel: "",
    streetAddress2: "",
    schoolDistrictID: 2,
    streetAddress1: "12 Eastway BLVD",
    state: "Texas",
    id: 1,
    name: "Northside High School",
  },
  {
    zip: 37043,
    city: "Clarksville",
    schoolDistrictName: "Lake Travis ISD",
    schoolDistrictID: 5,
    students: 43,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "1920 Madison St",
    state: "Tennessee",
    id: 6,
    name: "Barksdale Elementary School",
  },
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Lovejoy ISD",
    schoolDistrictID: 4,
    students: 10,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "301 Byron Nelson Pkwy",
    state: "Texas",
    id: 5,
    name: "Rockenbaugh Elementary School",
  },
  {
    zip: 76092,
    city: "Grapevine",
    schoolDistrictName: "Eanes ISD",
    schoolDistrictID: 3,
    students: 34,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "400 S Kimball Ave",
    state: "Texas",
    id: 4,
    name: "Dawson Middle School",
  },
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Carroll ISD",
    schoolDistrictID: 2,
    students: 20,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "800 N. White Chapel Boulevard",
    state: "Texas",
    id: 3,
    name: "Carroll High School",
  },
  {
    zip: 76092,
    city: "Southlake",
    schoolDistrictName: "Friendswood ISD",
    schoolDistrictID: 10,
    students: 12,
    personnel: "",
    streetAddress2: "",
    streetAddress1: "Friendswood Intermediate School",
    state: "Texas",
    id: 10,
    name: "Friendswood Intermediate School",
  },
];

async function insertUsers() {
  let insertPromises = ticketDetails.map(async (participant) => {
    let item = {
      Id: {
        S: participant["id"].toString(),
      },
      Name: {
        S: participant["name"].toString(),
      },
      StreetAddress1: {
        S: participant["streetAddress1"].toString(),
      },
      StreetAddress2: {
        S: participant["streetAddress2"].toString(),
      },
      City: {
        S: participant["city"].toString(),
      },
      State: {
        S: participant["state"].toString(),
      },
      Zip: {
        S: participant["zip"].toString(),
      },
      SchoolDistrictName: {
        S: participant["schoolDistrictName"].toString(),
      },
      SchoolDistrictID: {
        S: participant["schoolDistrictID"].toString(),
      },
      Personnel: {
        S: participant["personnel"].toString(),
      },
      Students: {
        S: participant["students"].toString(),
      },
    };

    await ddb
      .putItem({
        TableName: userTable,
        Item: item,
      })
      .promise();
  });

  await Promise.all(insertPromises);

  console.log("Done");
}

insertUsers();
